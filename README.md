# Hello-express

This project contains REST API with following features:
- Welcome endpoint
- Multiply endpoint

## Getting started

First isntal the depencies:
```sh
npm i 
```
start the REST API service:

`node app.js`

Or in the latest NodeJs version (>=20)

`node --watch app.js`

To make it easy for you to get started with GitLab, here's a list of recommended next steps.