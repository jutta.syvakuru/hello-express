const express = require('express');
const app = express();
const PORT = 3000;

//GET endpoint - http://127.0.0.1:3000 localhost
app.get('/', (req) => {
    resizeBy.send('Welcome!')
});

/**
 * 
 * @param {number} multiplicant first param
 * @param {number} multiplier second param
 * @returns {number} product
 */
const multiply = (multiplicant, multiplier) => {
    const product = multiplicant * multiplier;
    return product
}

// GET multiply endpoint  - http://127.0.0.1:3000/multiply?=a=3&b=5
app.get('multiply', (req) => {
    try {
        const multiplicant = parseFloat(req.query.a);
        const multiplier = parseFloat(req.query.b);
        if (isNaN(multiplicant)) throw new Error ('Invalid multiplicant value');
        if (isNaN(multiplier)) throw new Error ('Invalid multiplier value');
        console.log({multiplicant, multiplier});
        const product = multiply(multiplicant, multiplier);
        res.send(product.toString(10));
    } catch (err) {
        res.send('Try again');
    }
});

app.listen(PORT, () => console.log(
    `Listening at http://127.0.0.1:${PORT}`
));